package com.nx.arch.addon.lock.lock;

import com.nx.arch.addon.lock.NxLock;
import com.nx.arch.addon.lock.NxLockClient;
import com.nx.arch.addon.lock.client.RedisClient;
import com.nx.arch.addon.lock.exception.LockException;

/**
 * @类名称 RedisSolution.java
 * @类描述 redis 解决方案
 * @作者  庄梦蝶殇 linhuaichuan@naixuejiaoyu.com
 * @创建时间 2020年4月7日 下午4:12:40
 * @版本 1.0.0
 *
 * @修改记录
 * <pre>
 *     版本                       修改人 		修改日期 		 修改内容描述
 *     ----------------------------------------------
 *     1.0.0 		庄梦蝶殇 	2020年4月7日             
 *     ----------------------------------------------
 * </pre>
 */
public class RedisSolution implements NxLockClient {
    /**
     * redis客户端，基于jedis 实现
     */
    private RedisClient client;
    
    @Override
    public NxLockClient build() {
        return this;
    }
    
    @Override
    public NxLock newLock(String lockKey)
        throws LockException {
        return newLock(lockKey, false);
    }
    
    @Override
    public NxLock newLock(String lockKey, boolean reentrant)
        throws LockException {
        client = RedisClient.createInstance();
        return new RedisLock(client, lockKey, reentrant);
    }
    
    @Override
    public void close()
        throws LockException {
    }
    
}
