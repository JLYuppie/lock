package com.nx.arch.addon.lock;

import com.nx.arch.addon.lock.exception.LockException;

/**
 * @类名称 NxLock.java
 * @类描述 分布式锁对象
 * @作者  庄梦蝶殇 linhuaichuan@naixuejiaoyu.com
 * @创建时间 2020年3月28日 下午4:04:45
 * @版本 1.0.0
 *
 * @修改记录
 * <pre>
 *     版本                       修改人 		修改日期 		 修改内容描述
 *     ----------------------------------------------
 *     1.0.0 		庄梦蝶殇 	2020年3月28日             
 *     ----------------------------------------------
 * </pre>
 */
public abstract class NxLock implements AutoCloseable {
    static NxLockFactory factory = new NxLockFactory();
    
    /**
     * @方法名称 acquire
     * @功能描述 获取锁，如果没得到，不阻塞
     * @param ttl 过期时间
     * @return
     * @throws LockException
     */
    public abstract boolean acquire(int ttl)
        throws LockException;
    
    /**
     * @方法名称 acquire
     * @功能描述 获取锁，直到超时
     * @param ttl 过期时间
     * @param interval 重试间隔时间
     * @param retryCount 重试次数
     * @return 
     * @throws LockException 锁异常
     */
    public abstract boolean acquire(int ttl, long interval, int retryCount)
        throws LockException;
    
    /**
     * 释放锁
     */
    public abstract void release()
        throws LockException;
    
    /**
     * 获取锁工厂
     */
    public static NxLockFactory factory() {
        return factory;
    }
}
