package com.nx.arch.lock;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.junit.Test;

import com.nx.arch.addon.lock.NxLock;
import com.nx.arch.addon.lock.NxLockClient;
import com.nx.arch.addon.lock.lock.EtcdSolution;

public class ThreadCreateTest {
    
    ExecutorService es = Executors.newFixedThreadPool(15);
    
    @Test
    public void createThread() {
        int i = 0;
        long outStart = System.currentTimeMillis();
        while (i++ < 10000) {
            
// es.execute(() -> {
            long start = System.currentTimeMillis();
            Thread t = new Thread(() -> {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            });
            t.start();
            long cost = System.currentTimeMillis() - start;
            if (cost >= 1) {
                System.out.println(cost);
            }
// });
            
// try {
// TimeUnit.MICROSECONDS.sleep(100);
// } catch (InterruptedException e) {
// e.printStackTrace();
// }
        }
        System.out.println("qps " + i * 1000 / (System.currentTimeMillis() - outStart));
        
    }
    
    @Test
    public void lock()
        throws Exception {
        NxLockClient lockClient = new EtcdSolution().clusterName("test").connectionString("http://192.168.188.50:12379").timeout(5).build();
        NxLock lock = lockClient.newLock("test");
        System.out.println(lock.acquire(20));
    }
    
}
