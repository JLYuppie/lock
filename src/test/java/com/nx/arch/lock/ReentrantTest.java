package com.nx.arch.lock;

import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nx.arch.addon.lock.NxLock;
import com.nx.arch.addon.lock.NxLockClient;
import com.nx.arch.addon.lock.exception.LockException;

public class ReentrantTest {
    
    private static final Logger log = LoggerFactory.getLogger(ReentrantTest.class);
    private static NxLockClient LCOK_CLIENT;
    private static String testAddr = "http://192.168.188.49:2379";
    static String lockName = "lockname2";
    
    static {
        try {
            LCOK_CLIENT = NxLock.factory().etcdSolution().connectionString(testAddr.split(","))
                    .clusterName("monitor-server").build();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    static class HoldLockThread extends Thread {    
        @Override
        public void run() {
            try {
                NxLock lock = LCOK_CLIENT.newLock(lockName, true);
                if(lock.acquire(20)) {
                    log.info("HoldThread 获得锁...");
                    while(true) {
                        TimeUnit.SECONDS.sleep(1);
                        NxLock lock1 = LCOK_CLIENT.newLock(lockName, true);
                        if(lock1.acquire(20)) {
                            log.info("HoldThread 内部重入成功");
                            lock1.release();
                        } else {
                            log.error("HoldThread 内部重入失败");
                        }
                    }
                }
            } catch (LockException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
    
    static class OtherThread extends Thread {    
        @Override
        public void run() {
            try {
                while(true) {
                    NxLock lock = LCOK_CLIENT.newLock(lockName);
                    if(lock.acquire(20)) {
                        log.error("OtherThread 获得锁...");
                        lock.release();
                    }else {
                        log.info("OtherThread 未获得锁");
                    }
                    TimeUnit.SECONDS.sleep(1);
                }
            } catch (LockException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
    
    
    public static void main(String[] args) throws InterruptedException {
        
        new HoldLockThread().start();
        TimeUnit.SECONDS.sleep(10L);
        new OtherThread().start();
        
        TimeUnit.HOURS.sleep(10L);
        
        
    }

}
