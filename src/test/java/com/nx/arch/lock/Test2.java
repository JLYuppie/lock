package com.nx.arch.lock;

import com.nx.arch.addon.lock.NxLock;
import com.nx.arch.addon.lock.NxLockClient;


public class Test2 {
	public static void main(String[] args) throws Exception {
		NxLockClient nxClient = NxLock.factory().etcdSolution()
				  .connectionString("http://192.168.188.49:2379")
				  .clusterName("monitor-server")
				  .build();
		try(NxLock lock = nxClient.newLock("testLock")){
			System.err.println(lock.acquire(10));
			//Thread.sleep(1000*5);
		} catch (Exception e) {
		}
		
		NxLock lock2 = nxClient.newLock("testLock2");
		System.err.println(lock2.acquire(2,1,1));
		for(int i = 0 ; i < 500 ; i++){
			if(lock2.acquire(2)){
				System.err.println(i+"*****我获得了锁*****");
			}
		}
	}
}
